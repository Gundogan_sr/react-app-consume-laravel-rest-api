This is a React JS Application built on top of Admin LTE to show manufacturering status of different devices. Its embedded with the following functions.

- Responsive Items Dashboard
- View Item details on different route
- Delete an Item from the table
- Edit Item details by navigating to edit route.
- Add Manufacturer details 

### Clone the App from github

### Install Application dependencies

```bash
cd `smart-device`
npm install or yarn if you prefer the later

Ensure you have node v5+ installed first
```

### Run React from root

```bash
npm start or yarn start
```
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the browsers console.

### Build for production

```bash
cd `smart-device`
npm run build
```

## This application uses the following Api to make queries

- Use Postman to test the this APi[Devices APi](https://heroku.com/ujingene/SmartDevices).

- Manufacturers APi [Manufacturers Api](https://heroku.com/ujingene/Manufacturers)


## App Info

### Author

Eugene Wanjira
[Eugene Wanjira](http://www.github.com/ujingene)

### Version

1.0.0

### License

This project is licensed under the MIT License